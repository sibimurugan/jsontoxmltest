# JsonToXMLTest

Coding Test for converting Json to XML 

This application loads the test.json from it's classpath and converts it into XML then drops into same location as convertedXML.xml.   

I have created a Spring Boot application just to add the dependencies. It has single functional class with main method, If we execute the main method, It'll load the json and converts it into xml using JaxB java XML API. 


Steps to setup - 

- clone the project in IDE.
- make a maven clean install
- run the main method in WriteXMLDom.java
 
