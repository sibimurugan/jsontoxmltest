package com.json.to.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class WriteXmlDom {
	
	static final String object = "object";
	static final String string = "string";
	static final String number = "number";
	static final String nul = "null";
	static final String array = "array";
	static final String boolen = "boolean";
	
	static ObjectMapper objectMapper;
	static DocumentBuilderFactory docFactory;
	static DocumentBuilder docBuilder;
	
	static boolean mapCompleted = false;
	static boolean mapExecuted = true;
	
	static {
		 objectMapper = new ObjectMapper();

		 docFactory = DocumentBuilderFactory.newInstance();

			 try {
				docBuilder = docFactory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

     // root elements
	   static Document doc = docBuilder.newDocument();
    public static void main(String[] args)
            throws ParserConfigurationException, TransformerException, JsonMappingException, JsonProcessingException {
    	
    	
    	FileReader fr = null;
    	StringBuilder jsonStringBuilder = new StringBuilder();
		try {
			fr = new FileReader("input.json");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}    
        int i;    
        try {
			while((i=fr.read())!=-1) {    
				jsonStringBuilder.append((char)i);
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}    
        try {
			fr.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}    
    	
       String jsonString = jsonStringBuilder.toString();
    	/**String jsonString = "{\n" + 
    			"    \"organization\" : {\n" + 
    			"        \"name\" : \"RiskSense\",\n" + 
    			"        \"type\" : \"Inc\",\n" + 
    			"        \"building_number\" : 4,\n" + 
    			"        \"floating\" : -17.4,\n" + 
    			"        \"null_test\": null\n" + 
    			"    },\n" + 
    			"    \"security_related\" : true,\n" + 
    			"    \"array_example0\" : [\"red\", \"green\", \"blue\", \"black\"],\n" + 
    			"    \"array_example1\" : [1, \"red\", [{ \"nested\" : true}], { \"obj\" : false}]\n" + 
    			"}";**/
    	
    	
    	@SuppressWarnings("unchecked")
		LinkedHashMap<String, Object> result =
    			objectMapper.readValue(jsonString, LinkedHashMap.class);
    	
       
        Element rootElement = doc.createElement("object");
        doc.appendChild(rootElement);

        
        for (Entry<String, Object> entry : result.entrySet()) {
        	String key = entry.getKey();
        	Object obj = entry.getValue();
        	
            if(mapExecuted) {
            	Element ele = doc.createElement(entry.getKey());
            	ele = doc.createElement("object");
                rootElement.appendChild(ele);
            	checkInstance(obj,rootElement,ele, entry.getKey(),entry, false);
            }else if(mapCompleted) {
            	Element ele = doc.createElement(getInstance(obj));
            	rootElement.appendChild(ele);
            	checkInstance(obj,rootElement,ele, key,entry, false);
            }
	    }
        
        
        // print XML to system console
        writeXml(doc, System.out);

        try (FileOutputStream output =
                     new FileOutputStream("convertedXML.xml")) {
            writeXml(doc, output);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


	private static String getInstance(Object obj) {
		if(obj == null) {
			return "null";
		}
		
    	if(obj instanceof String) {
    		return string;
    	}else if(obj instanceof Integer) { 
    		
    		return number;
    	}
    	else if(obj instanceof ArrayList) {
    		return "array";
    	}else if(obj instanceof Boolean) {
    		return "boolean";
    		
    	}else if(obj instanceof Map) {
    		return "map";
    	}
		return "null";
    	
	
	}


	private static void checkInstance(Object obj, Element rootElement,Element eleMap,String key,Entry ent, boolean isArray) {
		
		if(obj == null) {
    		String s= (String) obj;
    		Element ele = doc.createElement("null");
    		ele.setAttribute("name", (String) key);
    		eleMap.appendChild(ele);
    	}else if(obj instanceof String) {
    		String s= (String) obj;
    		Element ele = doc.createElement(string);
    		
    		if(!isArray) {
    			ele.setAttribute("name", key);
        	}
            ele.setTextContent(s);
            if(eleMap!=null) {
            	eleMap.setAttribute("name", (String) ent.getKey());
            	eleMap.appendChild(ele);
            }else {
            	 rootElement.setAttribute("name", (String) ent.getKey());
            	 rootElement.appendChild(ele);
            }
            
            
            
    	}else if(obj instanceof Integer) {
    		Integer i = (Integer)obj;
    		String s= Integer.toString(i);
    		Element ele = doc.createElement(number);
    		if(!isArray) {
    			ele.setAttribute("name", key);
    		}
            ele.setTextContent(s);
            if(eleMap!=null) {
            	eleMap.appendChild(ele);
            }else {
            	 rootElement.appendChild(ele);
            }
    	}else if(obj instanceof Float) {
    		Float i = (Float)obj;
    		String s= Float.toString(i);
    		Element ele = doc.createElement(number);
            ele.setAttribute("name", key);
            ele.setTextContent(s);
            if(eleMap!=null) {
            	eleMap.appendChild(ele);
            }else {
            	 rootElement.appendChild(ele);
            }
    		
    	}else if(obj instanceof Double) {
    		Double i = (Double)obj;
    		String s= Double.toString(i);
    		Element ele = doc.createElement(number);
            ele.setAttribute("name", key);
            ele.setTextContent(s);
            if(eleMap!=null) {
            	eleMap.appendChild(ele);
            }else {
            	 rootElement.appendChild(ele);
            }
    		
    	}
    	
    	
    	else if(obj instanceof Boolean) {
    		Boolean i = (Boolean)obj;
    		String s= Boolean.toString(i);
    		Element ele = doc.createElement("boolean");
            ele.setAttribute("name", key);
            ele.setTextContent(s);
            
            if(eleMap!=null) {
            	eleMap.appendChild(ele);
            }else {
            	 rootElement.appendChild(ele);
            }
    		
    	}
    	
    	
    	
    	else if(obj instanceof ArrayList) {
    		ArrayList<Object> sa= (ArrayList) obj;
    		  for (Object entry1 : sa ) {
    			 String arayElement =  getInstance(entry1);
    			 if(arayElement=="map") {
    				 mapExecuted = true;
    			 }
    			 if(arayElement=="array") {
    				 
    				 Element ele = doc.createElement("array");
    		            ele.setAttribute("name", array);
    		            
    		            //checkInstance(entry1,rootElement,eleMap,arayElement,ent,false); 
    		            
    				 callArrayInsideArray(entry1,ele,arayElement);
    				 eleMap.appendChild(ele);
    				 
    			 }else {
    				 checkInstance(entry1,rootElement,eleMap,entry1.toString(),ent,false);
    				 rootElement.appendChild(eleMap);
    			 }
    		  }
    	}
    	
    	
    	else if(obj instanceof Map) {
    		if(mapExecuted) {
    			Map<String,Object> m = (Map<String, Object>) obj;
    			Entry<String, Object> entry = m.entrySet().iterator().next();
    			for (Entry<String, Object> entry1 : m.entrySet()) {
    				Object o = entry1.getValue();
    				String k = entry1.getKey();
    				if(isArray) {
    					checkInstance(o,rootElement,eleMap,k,ent,true);
    				}else {
    					checkInstance(o,rootElement,eleMap,k,ent,false);
    				}
    			}
    		}
    		mapExecuted = false;
    		mapCompleted = true;
    	}
	}
    
    

	private static void callArrayInsideArray(Object obj, Element rootElement,String string2) {
		if(obj == null) {
			String s= (String) obj;
    		Element ele = doc.createElement("null");
    		ele.setAttribute("name", (String) string2);
    		rootElement.appendChild(ele);
    	}else if(obj instanceof String) {
    		String s= (String) obj;
    		Element ele = doc.createElement(string);
    		
    			ele.setAttribute("name", string2);
            ele.setTextContent(s);
            if(rootElement!=null) {
            	rootElement.setAttribute("name", string2);
            	rootElement.appendChild(ele);
            }
            
            
    	}else if(obj instanceof Integer) {
    		Integer i = (Integer)obj;
    		String s= Integer.toString(i);
    		Element ele = doc.createElement(number);
    		
    			ele.setAttribute("name", string2);
    		
            ele.setTextContent(s);
            
            	 rootElement.appendChild(ele);
    	}else if(obj instanceof Double) {
    		Double i = (Double)obj;
    		String s= Double.toString(i);
    		Element ele = doc.createElement(number);
            ele.setAttribute("name", string2);
            ele.setTextContent(s);
            rootElement.appendChild(ele);
    		
    	}
    	
    	
    	else if(obj instanceof Boolean) {
    		Boolean i = (Boolean)obj;
    		String s= Boolean.toString(i);
    		Element ele = doc.createElement("boolean");
            ele.setAttribute("name", string2);
            ele.setTextContent(s);
            rootElement.appendChild(ele);
    	}
		
		
    	else if(obj instanceof Map) {
    			Map<String,Object> m = (Map<String, Object>) obj;
    			Entry<String, Object> entry = m.entrySet().iterator().next();
    			for (Entry<String, Object> entry1 : m.entrySet()) {
    				Object o = entry1.getValue();
    				String k = entry1.getKey();
    				checkInstance(entry1,rootElement,rootElement,"array",entry1,false); 
    			}
    	}else if(obj instanceof ArrayList) {
    		ArrayList<Object> sa= (ArrayList) obj;
    		for (Object entry1 : sa ) {
  			 String arayElement =  getInstance(entry1);
  			 if(arayElement=="map") {
  				 mapExecuted = true;
  			 }
  				 Element ele = doc.createElement("array");
  		            ele.setAttribute("name", array);
  				 callArrayInsideArray(entry1,ele,arayElement);
  		  }
  	}
		
		
	}


	// write doc to output stream
    private static void writeXml(Document doc,
                                 OutputStream output)
            throws TransformerException {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);

    }
}
